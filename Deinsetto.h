#ifndef DEINSETTO_H_INCLUDED
#define DEINSETTO_H_INCLUDED

#define VERBOSE 0
#define INFO 1
#define WARNING 3
#define ERROR 4
#define CRITICAL 6

void deinsetto_msg(int Livello, char * Messaggio);

int deinsetto_set(int Livello);

void deinsetto_enable(int Livello);

void deinsetto_disable();

int deinsetto_file(char * FileName);

int deinsetto_get();

void deinsetto_push(int Livello);

int deinsetto_pop();

#endif // DEINSETTO_H_INCLUDED
