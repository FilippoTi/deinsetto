//Serradura e Tiberio
#include "Deinsetto.h"
#include <stdio.h>
#include <string.h>

#define VERBOSE 0
#define INFO 1
#define WARNING 3
#define ERROR 4
#define CRITICAL 6

int deinsetto_livellodidebug = 4;
int deinsetto_enabled = 0;

int deinsetto_stack[10];
int deinsetto_index = -1;

FILE *deinsetto_FileDescriptor;
int deinsetto_isFileOut = 0;

void deinsetto_msg(int Livello, char * Messaggio)
{
    int internal_error = 0;

    if(Livello != VERBOSE && Livello != INFO && Livello != WARNING && Livello != ERROR && Livello != CRITICAL)
    {
        internal_error = 1;
    }

    if(internal_error != 0)
    {
        fprintf(deinsetto_FileDescriptor, "\nDebug %d> LIVELLO NON ESISTENTE\n", Livello);
        return;
    }

    if(deinsetto_enabled != 0 && Livello >= deinsetto_livellodidebug)
    {
        fprintf(deinsetto_FileDescriptor,"\nDebug %d> ", Livello);
        fprintf(deinsetto_FileDescriptor,"%s\n", Messaggio);
    }

    return;
}

int deinsetto_set(int Livello)
{
    if(Livello != VERBOSE && Livello != INFO && Livello != WARNING && Livello != ERROR && Livello != CRITICAL)
    {
        deinsetto_msg(WARNING, "\nDebug %d> LIVELLO NON ESISTENTE\n");
        return -1;
    }
    else
    {
        deinsetto_livellodidebug = Livello;
    }

    return 0;
}

void deinsetto_enable(int Livello)
{
    int isValid = deinsetto_set(Livello);
    if(isValid == 0)
    {
        deinsetto_enabled = 1;
    }
    deinsetto_FileDescriptor = stderr;
}

void deinsetto_disable()
{
    deinsetto_enabled = 0;
}

int deinsetto_file(char * FileName)
{
    if(strcasecmp(FileName, "")==0)
    {
        if(deinsetto_FileDescriptor != stderr && deinsetto_FileDescriptor)
        {
            fflush(deinsetto_FileDescriptor);
            fflush(stderr);
            fclose(deinsetto_FileDescriptor);
        }
        deinsetto_FileDescriptor = stderr;
        deinsetto_isFileOut = 0;
    }
    else
    {
        deinsetto_FileDescriptor = fopen(FileName, "a");
        if(deinsetto_FileDescriptor)
        {
            deinsetto_isFileOut = 1;
        }
        else
        {
            deinsetto_isFileOut = 0;
            deinsetto_FileDescriptor = stderr;
        }
    }

    return 0;
}

int deinsetto_get()
{
    return deinsetto_livellodidebug;
}

void deinsetto_push(int Livello)
{
    if(deinsetto_index >= 9)
    {
        fprintf(deinsetto_FileDescriptor, "\nStack Pieno!\n");
        return;
    }

    deinsetto_index++;
    deinsetto_stack[deinsetto_index] = deinsetto_get();
    int isValid = deinsetto_set(Livello);
    if(isValid != 0)
    {
        deinsetto_set(deinsetto_pop());
    }

    return;
}

int deinsetto_pop()
{
    if(deinsetto_index >= 0)
    {
        deinsetto_index--;
        deinsetto_livellodidebug = deinsetto_stack[deinsetto_index+1];
        return deinsetto_stack[deinsetto_index+1];
    }
    else
    {
        fprintf(deinsetto_FileDescriptor, "\nStack Vuoto\n");
        return -1;
    }
}
